package com.bruno.springdata.services;

import java.sql.Date;
import java.time.Instant;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bruno.springdata.entities.StudentEntity;
import com.bruno.springdata.repositories.student.StudentRepository;

@Service
public class StudentService implements IStudentService {
	@Autowired
	private StudentRepository studentRepository;

	@Override
	public StudentEntity getById(Long id) {
		List<StudentEntity> findByStudentEntityIdentityIdAndStudentEntityIdentityInsertDateGreaterThanEqual = this.studentRepository
				.findAll();
		return findByStudentEntityIdentityIdAndStudentEntityIdentityInsertDateGreaterThanEqual.get(0);
	}

	@Override
	public List<StudentEntity> getByIdAndIdCourse(Long id) {

		return this.studentRepository.finbByIdAndIdCourseTris(id, Date.from(Instant.now()));
	}

	@Override
	public StudentEntity createStudent(StudentEntity studentEntity) {
		return this.studentRepository.save(studentEntity);
	}

}
