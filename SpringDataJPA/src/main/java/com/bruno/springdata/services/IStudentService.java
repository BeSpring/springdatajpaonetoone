package com.bruno.springdata.services;

import java.util.List;

import com.bruno.springdata.entities.StudentEntity;

public interface IStudentService {
	public StudentEntity getById(Long id);
	public List<StudentEntity> getByIdAndIdCourse(Long id);
	public StudentEntity createStudent(StudentEntity studentEntity);

}
