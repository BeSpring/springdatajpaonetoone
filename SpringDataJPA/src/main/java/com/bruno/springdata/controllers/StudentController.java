package com.bruno.springdata.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bruno.springdata.entities.StudentEntity;
import com.bruno.springdata.entities.StudentEntityIdentity;
import com.bruno.springdata.repositories.student.StudentRepository;
import com.bruno.springdata.services.IStudentService;

@RestController
@RequestMapping("/studentcontroller")
public class StudentController {

	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private IStudentService iStudentService;

	@PostMapping("/getstudentbyid")
	public Optional<StudentEntity> getStudentById(@RequestBody StudentEntity studentEntity) {
		return this.studentRepository.findById(studentEntity.getId());

	}

	@GetMapping("/getstudentbyidandcourse")
	public StudentEntity getStudentByIdAndCourse() {
		return this.studentRepository.finbByIdAndIdCourse();

	}

	@PostMapping("/getstudentbyidandcoursebis")
	public List<StudentEntity> getStudentByIdAndCourseBis(@RequestBody StudentEntityIdentity studentEntityIdentity) {
		return this.studentRepository.finbByIdAndIdCourseBis(studentEntityIdentity.getId(),
				studentEntityIdentity.getInsertDate());

	}

	@PostMapping("/getstudentbyidandinsertdate")
	public List<StudentEntity> getStudentByIdAndInsertDate(@RequestBody StudentEntity studentEntity) {
		return this.studentRepository.findByIdAndInsertDateGreaterThanEqual(studentEntity.getId(),
				studentEntity.getInsertDate());

	}

	@PostMapping("/getstudentbyidanddateservice")
	public StudentEntity getStudentByIdAndDateService(@RequestBody StudentEntityIdentity studentEntityIdentity) {
		return this.iStudentService.getById(studentEntityIdentity.getId());

	}

	@PostMapping("/getstudentbyidandcoursetris")
	public List<StudentEntity> getStudentByIdAndCourseTris(@RequestBody StudentEntityIdentity studentEntityIdentity) {
		return this.iStudentService.getByIdAndIdCourse(studentEntityIdentity.getId());

	}

	@PostMapping("/createstudent")
	public StudentEntity createStudent(@RequestBody StudentEntity studentEntity) {
		return this.iStudentService.createStudent(studentEntity);

	}

}
