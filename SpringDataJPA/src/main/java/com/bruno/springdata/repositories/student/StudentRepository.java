package com.bruno.springdata.repositories.student;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bruno.springdata.entities.StudentEntity;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
	
	@Query(value = "SELECT * FROM STUDENT s\r\n" + "JOIN COURSE c ON s.idstudent\r\n"
			+ "WHERE c.idstudent=s.idstudent", nativeQuery = true)
	StudentEntity finbByIdAndIdCourse();

	@Query(value = "SELECT * FROM STUDENT s\r\n" + "JOIN COURSE c ON s.idstudent\r\n"
			+ "WHERE c.idstudent=s.idstudent AND c.idstudent=:id AND s.insert_date >= :date", nativeQuery = true)
	List<StudentEntity> finbByIdAndIdCourseBis(@Param(value = "id") Long id, @Param(value = "date") Date date);
	
	@Query(value = "SELECT s.id,s.insertDate,s.name,c.idCourse FROM StudentEntity s JOIN s.courseEntity c ON s.id=c.id WHERE s.id=:id AND s.insertDate >= :date")
	List<StudentEntity> finbByIdAndIdCourseTris(@Param(value = "id") Long id, @Param(value = "date") Date date);

	List<StudentEntity> findByIdAndInsertDateGreaterThanEqual(Long id, Date date);
	

}