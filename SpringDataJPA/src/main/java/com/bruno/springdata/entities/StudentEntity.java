package com.bruno.springdata.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "student")
@IdClass(value = StudentEntityIdentity.class)
public class StudentEntity {
	@Id
	@Column(name = "idstudent")
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date insertDate;
	private String name;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(insertable=false,updatable=false,name= "idstudent", referencedColumnName = "idstudent")
	private CourseEntity courseEntity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CourseEntity getCourseEntity() {
		return courseEntity;
	}

	public void setCourseEntity(CourseEntity courseEntity) {
		this.courseEntity = courseEntity;
	}

	public StudentEntity() {
	}

	public StudentEntity(Long id, Date insertDate, String name) {
		this.id = id;
		this.insertDate = insertDate;
		this.name = name;
	}

	public StudentEntity(Long id, Date insertDate, String name, CourseEntity courseEntity) {
		this.id = id;
		this.insertDate = insertDate;
		this.name = name;
		this.courseEntity = courseEntity;
	}
	

}
