package com.bruno.springdata;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bruno.springdata.entities.StudentEntity;
import com.bruno.springdata.repositories.student.StudentRepository;

@SpringBootApplication
public class SpringDataJpaApplication implements CommandLineRunner {
	@Autowired
	private StudentRepository studentRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		this.studentRepository.save(new StudentEntity(1L, Date.from(Instant.now()), "Adriano"));
	}

}
